import java.awt.*;

class ChatDialog extends Dialog {

   /**
    * 
    */
   private static final long serialVersionUID = 1L;
   public final static int MSG_TYPE_UNDEFINED = -1;
   public final static int MSG_TYPE_TEXT = 1;

   private TextField nameF, topicF;
   private Choice msgTypeChoice;
   private Button connectB, cancelB;

   /**
    * ChatDialog constructor.
    * 
    * @param f
    *           Parent frame.
    */
   public ChatDialog(Frame f) {
      super(f, "Simple Chat: Connect information", true);
      init();
      setResizable(false);
   }

   /**
    * Return 'Connect' button
    */
   public Button getConnectButton() {
      return (connectB);
   }

   /**
    * Return 'Cancel' button
    */
   public Button getCancelButton() {
      return (cancelB);
   }

   /**
    * Return chat user name entered.
    */
   public String getChatUserName() {
      if (nameF == null)
         return (null);
      return (nameF.getText());
   }

   /**
    * Set chat user name.
    * 
    * @param s
    *           chat user name
    */
   public void setChatUserName(String s) {
      if (nameF == null)
         return;
      nameF.setText(s);
   }

   /**
    * Set chat topic
    * 
    * @param s
    *           chat topic
    */
   public void setChatTopicName(String s) {
      if (topicF == null)
         return;
      topicF.setText(s);
   }

   /**
    * Return chat topic
    */
   public String getChatTopicName() {
      if (topicF == null)
         return (null);
      return (topicF.getText());
   }

   /*
    * Get message type
    */
   public int getMsgType() {
      if (msgTypeChoice == null)
         return (MSG_TYPE_UNDEFINED);
      return (msgTypeChoice.getSelectedIndex());
   }

   public String getMsgTypeString() {
      if (msgTypeChoice == null)
         return (null);
      return (msgTypeChoice.getSelectedItem());
   }

   /*
    * Init GUI elements.
    */
   private void init() {
      Panel p, dummyPanel, labelPanel, valuePanel;
      GridBagLayout labelGbag, valueGbag;
      GridBagConstraints labelConstraints, valueConstraints;
      Label chatNameLabel, chatTopicLabel, msgTypeLabel;
      int i, j;

      p = new Panel();
      p.setLayout(new BorderLayout());

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout());

      /***/
      labelPanel = new Panel();
      labelGbag = new GridBagLayout();
      labelConstraints = new GridBagConstraints();
      labelPanel.setLayout(labelGbag);
      j = 0;

      valuePanel = new Panel();
      valueGbag = new GridBagLayout();
      valueConstraints = new GridBagConstraints();
      valuePanel.setLayout(valueGbag);
      i = 0;

      chatNameLabel = new Label("Chat User Name:", Label.RIGHT);
      chatTopicLabel = new Label("Chat Topic:", Label.RIGHT);
      msgTypeLabel = new Label("Outgoing Msg Type:", Label.RIGHT);

      labelConstraints.gridx = 0;
      labelConstraints.gridy = j++;
      labelConstraints.weightx = 1.0;
      labelConstraints.weighty = 1.0;
      labelConstraints.anchor = GridBagConstraints.EAST;
      labelGbag.setConstraints(chatNameLabel, labelConstraints);
      labelPanel.add(chatNameLabel);

      labelConstraints.gridy = j++;
      labelGbag.setConstraints(chatTopicLabel, labelConstraints);
      labelPanel.add(chatTopicLabel);

      labelConstraints.gridy = j++;
      labelGbag.setConstraints(msgTypeLabel, labelConstraints);
      labelPanel.add(msgTypeLabel);

      nameF = new TextField(20);
      topicF = new TextField(20);
      msgTypeChoice = new Choice();
      msgTypeChoice.insert("TextMessage", MSG_TYPE_TEXT);
      msgTypeChoice.select(MSG_TYPE_TEXT);

      valueConstraints.gridx = 0;
      valueConstraints.gridy = i++;
      valueConstraints.weightx = 1.0;
      valueConstraints.weighty = 1.0;
      valueConstraints.anchor = GridBagConstraints.WEST;
      valueGbag.setConstraints(nameF, valueConstraints);
      valuePanel.add(nameF);

      valueConstraints.gridy = i++;
      valueGbag.setConstraints(topicF, valueConstraints);
      valuePanel.add(topicF);

      valueConstraints.gridy = i++;
      valueGbag.setConstraints(msgTypeChoice, valueConstraints);
      valuePanel.add(msgTypeChoice);

      dummyPanel.add("West", labelPanel);
      dummyPanel.add("Center", valuePanel);
      /***/

      p.add("North", dummyPanel);

      dummyPanel = new Panel();
      connectB = new Button("Connect");
      cancelB = new Button("Cancel");
      dummyPanel.add(connectB);
      dummyPanel.add(cancelB);

      p.add("South", dummyPanel);

      add(p);
      pack();
   }
}
