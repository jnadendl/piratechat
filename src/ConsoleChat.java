import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConsoleChat extends ChatBase implements MessageListener {

   boolean connected = false;

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public ConsoleChat() {
      super();
   }
   
   /*
    * To be overridden by dervied classes to output senders message.
    */
   @Override
   protected void outputMessage (String sender, int type, String msgText) {
      System.out.println(sender + ": " + msgText + "\n");
   }

   /**
    * Reads input from the console, and creates a message from it
    * If input is exit, the console is closed and exit is called
    */
   @Override
   protected String fetchInputMessage() throws Exception
   {
      // Read from command line
      BufferedReader commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));
      
      //check to see if the user wishes to exit
      String val = commandLine.readLine();
      if( val.toLowerCase() == "exit"){
         commandLine.close();
         commandLine = null;
         exit();
      }
      return val;
   }

   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    */
   public static void main(String[] args) {
      ConsoleChat sc = new ConsoleChat();

      sc.initializeActiveMq(args);
   }

}
