import java.awt.*;

class ChatPanel extends Panel implements ChatMessageTypes {
   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   private String destName, userName, hostName;

   private Button sendB;

   private Label destLabel, userLabel, msgsLabel, sendMsgLabel;

   private TextArea msgsTA;

   private TextArea sendMsgTA;

   /*
    * SimpleChatPanel constructor
    */
   public ChatPanel() {
      init();
   }

   /**
    * Set the chat username
    * 
    * @param userName
    *           Chat userName
    */
   public void setUserName(String userName) {
      this.userName = userName;
      userLabel.setText("User Id: " + this.userName);
      sendB.setLabel("Send Message as " + this.userName);
   }

   /**
    * Set the chat hostname. This is pretty much the host that the router is
    * running on.
    * 
    * @param hostName
    *           Chat hostName
    */
   public void setHostName(String hostName) {
      this.hostName = hostName;
      System.out.println(this.hostName);
   }

   /**
    * Sets the topic name.
    * 
    * @param destName
    *           Chat topic name
    */
   public void setDestName(String destName) {
      this.destName = destName;
      destLabel.setText("Topic: " + this.destName);
   }

   /**
    * Returns the 'Send' button.
    */
   public Button getSendButton() {
      return (sendB);
   }

   /**
    * Clears the chat message text area.
    */
   public void clear() {
      msgsTA.setText("");
   }

   /**
    * Appends the passed message to the chat message text area.
    * 
    * @param msg
    *           Message to display
    */
   public void newMessage(String sender, int type, String text) {
      switch (type) {
      case NORMAL:
         msgsTA.append(sender + ": " + text + "\n");
         break;

      case JOIN:
         msgsTA.append("*** " + sender + " has joined chat session\n");
         break;

      case LEAVE:
         msgsTA.append("*** " + sender + " has left chat session\n");
         break;

      default:
      }
   }

   /**
    * Sets the string to display on the chat message textarea
    * 
    * @param s
    *           String to display
    */
   public void setMessage(String s) {
      sendMsgTA.setText(s);
   }

   /**
    * Returns the contents of the chat message textarea
    */
   public String getMessage() {
      return (sendMsgTA.getText());
   }

   /*
    * Init chat panel GUI elements.
    */
   private void init() {

      Panel dummyPanel;

      setLayout(new BorderLayout(0, 0));

      destLabel = new Label("Topic:");

      userLabel = new Label("User Id: ");

      //msgTypeLabel = new Label("Outgoing Msg Type:");

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout(0, 0));
      dummyPanel.add("North", destLabel);
      dummyPanel.add("Center", userLabel);
      //dummyPanel.add("South", msgTypeLabel);
      add("North", dummyPanel);

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout(0, 0));
      msgsLabel = new Label("Messages in chat:");
      msgsTA = new TextArea(15, 40);
      msgsTA.setEditable(false);

      dummyPanel.add("North", msgsLabel);
      dummyPanel.add("Center", msgsTA);
      add("Center", dummyPanel);

      dummyPanel = new Panel();
      dummyPanel.setLayout(new BorderLayout(0, 0));
      sendMsgLabel = new Label("Type Message:");
      sendMsgTA = new TextArea(5, 40);
      sendB = new Button("Send Message");
      dummyPanel.add("North", sendMsgLabel);
      dummyPanel.add("Center", sendMsgTA);
      dummyPanel.add("South", sendB);
      add("South", dummyPanel);
   }
}
