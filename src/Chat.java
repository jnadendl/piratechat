import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class Chat extends ChatBase implements ActionListener, WindowListener {

   boolean connected = false;

   Frame frame;
   ChatPanel scp;
   ChatDialog scd = null;
   MenuItem clearItem, exitItem;
   Button sendB, connectB, cancelB;

   /**
    * Chat constructor. Initializes the chat user name, topic, hostname.
    */
   public Chat() {
      super();
   }

   public void actionPerformed(ActionEvent e) {
      Object obj = e.getSource();

      if (obj == sendB) {
         sendNormalMessage();
      } else if (obj == clearItem) {
         scp.clear();
      } else if (obj == exitItem) {
         exit();
      }
   }

   public void windowClosing(WindowEvent e) {
      e.getWindow().dispose();
   }

   public void windowClosed(WindowEvent e) {
      exit();
   }

   public void windowActivated(WindowEvent e) {
   }

   public void windowDeactivated(WindowEvent e) {
   }

   public void windowDeiconified(WindowEvent e) {
   }

   public void windowIconified(WindowEvent e) {
   }

   public void windowOpened(WindowEvent e) {
   }

   
   /*
    * To be overridden by dervied classes to output senders message.
    */
   @Override
   protected void outputMessage (String sender, int type, String msgText) {
      scp.newMessage(sender, type, msgText);
   }

   /*
    * END INTERFACE MessageListener
    */

   /*
    * Popup the ChatDialog to query the user for the chat user name and chat
    * topic.
    */
   private void queryForChatNames() {
      if (scd == null) {
         scd = new ChatDialog(frame);
         connectB = scd.getConnectButton();
         connectB.addActionListener(this);
         cancelB = scd.getCancelButton();
         cancelB.addActionListener(this);
      }

      scd.setChatUserName(name);
      scd.setChatTopicName(topicName);
      scd.setVisible(true);
   }

   /*
    * Performs the actual chat connect. The createChatSession() method does the
    * real work here, creating: Connection Session Topic MessageConsumer
    * MessageProducer
    */
   @Override
   protected void doConnect() {
      super.doConnect();
      scp.setUserName(name);
      scp.setDestName(topicName);
      scp.setHostName(hostName);
      scp.setEnabled(true);
   }

   /*
    * Disconnects from chat session. destroyChatSession() performs the JMS
    * cleanup.
    */
   @Override
   protected void doDisconnect() {
      super.doDisconnect();
      scp.setEnabled(false);
   }

   /*
    * Create the application GUI.
    */
   private void initGui() {

      frame = new Frame(" Chat");
      frame.addWindowListener(this);
      MenuBar menubar = createMenuBar();
      frame.setMenuBar(menubar);

      scp = new ChatPanel();
      scp.setUserName(name);
      scp.setDestName(topicName);
      scp.setHostName(hostName);
      sendB = scp.getSendButton();
      sendB.addActionListener(this);

      frame.add(scp);
      frame.pack();
      frame.setVisible(true);

      scp.setEnabled(false);
   }

   /*
    * Create menubar for application.
    */
   private MenuBar createMenuBar() {
      MenuBar mb = new MenuBar();
      Menu chatMenu;

      chatMenu = (Menu) mb.add(new Menu("Connection Spec"));

      clearItem = (MenuItem) chatMenu.add(new MenuItem("Clear Messages"));
      exitItem = (MenuItem) chatMenu.add(new MenuItem("Log Out"));

      clearItem.addActionListener(this);
      exitItem.addActionListener(this);

      return (mb);
   }

   
   /*
    * To be overriden by subclasses. Takes message input by user and sends it to server
    */
   @Override
   protected String fetchInputMessage() throws Exception{
      return scp.getMessage();
   }

   /**
    * @param args
    *           Arguments passed via the command line. These are used to create
    *           the ConnectionFactory.
    *           
    *           Usage: Chat <URL> <NAME> <TOPIC>
    */
   public static void main(String[] args) {
      
      Chat sc = new Chat();

      sc.initGui();
      sc.initializeActiveMq(args); 
   }

}
